import React, { useState, useEffect, useRef } from "react";
import {
  Row,
  Col,
  Form,
  Button,
  DatePicker,
  notification,
  Icon,
  Input,
} from "antd";
import moment from "moment";
import { Editor } from "@tinymce/tinymce-react";

import { getAccessTokenApi } from "../../../../api/auth";
import { appPostApi, updatePostApi } from "../../../../api/post";

import "./AddEditPostForm.scss";

export default function AddEditPostForm(props) {
  const { setIsVisibleModal, setReloadPosts, post } = props;
  const [postData, setPostData] = useState({});

  useEffect(() => {
    if (post) {
      setPostData(post);
    } else {
      setPostData({});
    }
  }, [post]);

  const processPost = (e) => {
    e.preventDefault();
    const { title, url, description, date } = postData;

    if (!title || !url || !description || !date) {
      notification["warning"]({
        message: "Todos los campos son obligatorios",
      });
    } else {
      if (!post) {
        addPost();
      } else {
        updatePost();
      }
    }
  };

  const addPost = () => {
    const AccessToken = getAccessTokenApi();
    appPostApi(AccessToken, postData)
      .then((response) => {
        const typeNotification = response.code === 200 ? "success" : "warning";
        notification[typeNotification]({
          message: response.message,
        });
        setIsVisibleModal(false);
        setReloadPosts(true);
        setPostData({});
      })
      .catch(() => {
        notification["error"]({
          message: "Error del servidor",
        });
      });
  };

  const updatePost = () => {
    const AccessToken = getAccessTokenApi();
    updatePostApi(AccessToken, post._id, postData)
      .then((response) => {
        const typeNotification = response.code === 200 ? "success" : "error";
        notification[typeNotification]({
          message: response.message,
        });
        setIsVisibleModal(false);
        setReloadPosts(true);
        setPostData({});
      })
      .catch(() => {
        notification["error"]({
          message: "Error del servidor",
        });
      });
  };

  return (
    <div className="add-edit-post-form">
      <AddEditForm
        postData={postData}
        setPostData={setPostData}
        post={post}
        processPost={processPost}
      />
    </div>
  );
}

function AddEditForm(props) {
  const { postData, setPostData, post, processPost } = props;
  const editorRef = useRef(null);

  return (
    <Form className="add-edit-post-form" layout="inline">
      <Row gutter={24}>
        <Col span={8}>
          <Input
            prefix={<Icon type="font-size" />}
            placeholder="Título"
            value={postData.title}
            onChange={(e) =>
              setPostData({ ...postData, title: e.target.value })
            }
          />
        </Col>
        <Col span={8}>
          <Input
            prefix={<Icon type="link" />}
            placeholder="Url"
            value={postData.url}
            onChange={(e) =>
              setPostData({
                ...postData,
                url: transformTextUrl(e.target.value),
              })
            }
          />
        </Col>
        <Col span={8}>
          <DatePicker
            style={{ width: "100%" }}
            format="DD/MM/YYYY HH:mm:ss"
            placeholder="Fecha de Publicación"
            showTime={{
              defaultValue: moment("00:00:00", "HH:mm:ss"),
            }}
            value={postData.date && moment(postData.date)}
            onChange={(e, value) =>
              setPostData({
                ...postData,
                date: moment(value, "DD/MM/YYYY HH:mm:ss").toISOString(),
              })
            }
          />
        </Col>
      </Row>

      {/* EDITOR */}
      <Editor
        // apiKey="4khs8obm9brhd2rgj4z68qbbkjfmy1m2pvup8g1sri4xqhqi"
        onInit={(evt, editor) => (editorRef.current = editor)}
        initialValue={postData.description ? postData.description : " "}
        init={{
          height: 400,
          menubar: true,
          plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste code help wordcount",
          ],
          toolbar:
            "undo redo | formatselect | " +
            "bold italic backcolor | alignleft aligncenter " +
            "alignright alignjustify | bullist numlist outdent indent | " +
            "removeformat | help",
          content_style:
            "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
        }}
        onBlur={(e) =>
          setPostData({
            ...postData,
            description: e.target.getContent(),
          })
        }
        // onChange={(e) => console.log(e.target.getContent())}
      />

      <Button
        type="primary"
        htmlType="submit"
        className="btn-submit"
        onClick={processPost}
      >
        {post ? "Actualizar Post" : "Crear Post"}
      </Button>
    </Form>
  );
}

function transformTextUrl(text) {
  const url = text.replace(" ", "-");
  return url.toLowerCase();
}
