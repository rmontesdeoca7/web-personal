import React from "react";
import { Row, Col, Card, Icon } from "antd";
import "./HowMyCoursesWork.scss";

export default function HowMyCoursesWork() {
  return (
    <Row className="how-my-courses-work">
      <Col lg={24} className="how-my-courses-work__title">
        <h2>Cómo funcionan mis cursos?</h2>
        <h3>
          Cada curso cuenta con contenido bajo la web de Udemy, activa las 24
          horas del día los 365 días del año.
        </h3>
      </Col>
      <Col lg={4} />
      <Col lg={16}>
        <Row className="row-cards">
          <Col md={8}>
            <CardInfo
              icon="clock-circle"
              title="Cursos y Clases"
              description="Cursos entre 10 y 30 horas y cada clase del curso con duración máxima de 15min."
            />
          </Col>
          <Col md={8}>
            <CardInfo
              icon="key"
              title="Acceso 24/7"
              description="Accede a todos los cursos en cualquier momento, desde cualquier lugar."
            />
          </Col>
          <Col md={8}>
            <CardInfo
              icon="message"
              title="Aprendizaje colaborativo"
              description="Aprende con los demás alumnos del curso, compartiendo sus conocimientos en comunidad."
            />
          </Col>
          <Col md={8}>
            <CardInfo
              icon="user"
              title="Mejora tu perfil"
              description="Mejora tu perfil para mantenerte actualizado"
            />
          </Col>
          <Col md={8}>
            <CardInfo
              icon="dollar"
              title="Precios bajos"
              description="Obten el curso por solo $9.99, por tiempo limitado."
            />
          </Col>
          <Col md={8}>
            <CardInfo
              icon="check-circle"
              title="Certificados de finalización"
              description="Obten tu certificado al terminar el curso."
            />
          </Col>
        </Row>
      </Col>
      <Col lg={4} />
    </Row>
  );
}

function CardInfo(props) {
  const { title, description, icon } = props;
  const { Meta } = Card;

  return (
    <Card className="how-my-courses-work__card">
      <Icon type={icon} />
      <Meta title={title} description={description} />
    </Card>
  );
}
