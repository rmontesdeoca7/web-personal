import React from "react";
import { ReactComponent as YoutubeIcon } from "../../../assets/img/social/youtube.svg";
import { ReactComponent as TwitterIcon } from "../../../assets/img/social/twitter.svg";
import { ReactComponent as FacebookIcon } from "../../../assets/img/social/facebook.svg";
import { ReactComponent as LinkedinIcon } from "../../../assets/img/social/linkedin.svg";

import "./SocialLinks.scss";

export default function SocialLinks() {
  return (
    <div className="social-links">
      <a
        href="https://youtube.com"
        className="youtube"
        target="_blank"
        rel="noopener noreferrer"
      >
        <YoutubeIcon />
      </a>

      <a
        href="https://linkedin.com"
        className="linkedin"
        target="_blank"
        rel="noopener noreferrer"
      >
        <LinkedinIcon />
      </a>

      <a
        href="https://Twitter.com"
        className="twitter"
        target="_blank"
        rel="noopener noreferrer"
      >
        <TwitterIcon />
      </a>
      <a
        href="https://Facebook.com"
        className="facebook"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FacebookIcon />
      </a>
    </div>
  );
}
