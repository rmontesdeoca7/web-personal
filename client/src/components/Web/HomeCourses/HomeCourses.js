import React from "react";
import { Row, Col, Card, Button } from "antd";
import { Link } from "react-router-dom";
import "./HomeCourses.scss";

import javascript from "../../../assets/img/cursos/javascript.jpg";
import node from "../../../assets/img/cursos/node.jpg";
import react from "../../../assets/img/cursos/react.png";
import wordpress from "../../../assets/img/cursos/wordpress.jpg";

export default function HomeCourses() {
  return (
    <Row className="home-courses">
      <Col className="home-courses__title" lg={24}>
        <h2>Aprende y mejora tus habilidades</h2>
      </Col>
      <Col lg={4} />
      <Col lg={16}>
        <Row className="row-courses">
          <Col md={6}>
            <CardCourse
              image={react}
              title="React JS Hook"
              subtitle="Intermedio - React/Javascript"
              link="https://udemy.com/react"
            />
          </Col>
          <Col md={6}>
            <CardCourse
              image={node}
              title="Node JS "
              subtitle="Intermedio - Node/Javascript"
              link="https://udemy.com/node"
            />
          </Col>
          <Col md={6}>
            <CardCourse
              image={javascript}
              title="Javascript JS "
              subtitle="Intermedio - Javascript"
              link="https://udemy.com/javascript"
            />
          </Col>
          <Col md={6}>
            <CardCourse
              image={wordpress}
              title="Wordpress PHP"
              subtitle="Avanzado - Wordpress"
              link="https://udemy.com/worpress"
            />
          </Col>
        </Row>
      </Col>
      <Col lg={4} />
      <Col lg={24} className="home-courses__more">
        <Link to={"/courses"}>
          <Button>Ver más</Button>
        </Link>
      </Col>
    </Row>
  );
}

function CardCourse(props) {
  const { image, title, subtitle, link } = props;
  const { Meta } = Card;

  return (
    <a href={link} target="_blank" rel="noopener noreferrer">
      <Card
        className="home-courses__card"
        cover={<img src={image} alt={title} />}
        actions={[<Button>Ingresa</Button>]}
      >
        <Meta title={title} description={subtitle} />
      </Card>
    </a>
  );
}
