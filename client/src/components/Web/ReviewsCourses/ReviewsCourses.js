import React from "react";
import { Row, Col, Card, Avatar } from "antd";

import "./ReviewsCourses.scss";
import AvatarPersona from "../../../assets/img/no-avatar.png";

export default function ReviewsCourses() {
  return (
    <Row className="reviews-courses">
      <Row>
        <Col lg={4} />
        <Col lg={16} className="reviews-courses__title">
          <h2>
            Forma parte de los +35 mil estudiantes que estan aprendiendo con mis
            cursos.
          </h2>
        </Col>
        <Col lg={4} />
      </Row>

      <Row>
        <Col lg={4} />
        <Col lg={16} className="row-cards">
          <Col md={8}>
            <CardReview
              name="Alonso Frias"
              subtitle="Alumno de Udemy"
              avatar={AvatarPersona}
              review="Un curso excelente, aprendi las bases de la programación aplicada a React, donde use los componentes funcionales y toda la parte reactiva de la programación."
            />
          </Col>
          <Col md={8}>
            <CardReview
              name="David Santos"
              subtitle="Alumno de Udemy"
              avatar={AvatarPersona}
              review="Un curso excelente, aprendi las bases de la programación aplicada a React, donde use los componentes funcionales y toda la parte reactiva de la programación."
            />
          </Col>
          <Col md={8}>
            <CardReview
              name="Juan Martin"
              subtitle="Alumno de Udemy"
              avatar={AvatarPersona}
              review="Un curso excelente, aprendi las bases de la programación aplicada a React, donde use los componentes funcionales y toda la parte reactiva de la programación."
            />
          </Col>
        </Col>
        <Col lg={4} />
      </Row>
      <Row>
        <Col lg={4} />
        <Col lg={16} className="row-cards">
          <Col md={8}>
            <CardReview
              name="Jesús Cross"
              subtitle="Alumno de Udemy"
              avatar={AvatarPersona}
              review="Un curso excelente, aprendi las bases de la programación aplicada a React, donde use los componentes funcionales y toda la parte reactiva de la programación."
            />
          </Col>
          <Col md={8}>
            <CardReview
              name="Martina López"
              subtitle="Alumno de Udemy"
              avatar={AvatarPersona}
              review="Un curso excelente, aprendi las bases de la programación aplicada a React, donde use los componentes funcionales y toda la parte reactiva de la programación."
            />
          </Col>
          <Col md={8}>
            <CardReview
              name="Francisco Reck"
              subtitle="Alumno de Udemy"
              avatar={AvatarPersona}
              review="Un curso excelente, aprendi las bases de la programación aplicada a React, donde use los componentes funcionales y toda la parte reactiva de la programación."
            />
          </Col>
        </Col>
        <Col lg={4} />
      </Row>
    </Row>
  );
}

function CardReview(props) {
  const { name, subtitle, avatar, review } = props;
  const { Meta } = Card;

  return (
    <Card className="reviews-courses__card">
      <p>{review}</p>
      <Meta
        avatar={<Avatar src={avatar} />}
        title={name}
        description={subtitle}
      />
    </Card>
  );
}
