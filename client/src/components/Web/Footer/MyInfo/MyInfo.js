import React from "react";
import "./MyInfo.scss";
import LogoWhite from "../../../../assets/img/logo.png";
import SocialLinks from "../../SocialLinks";

export default function MyInfo() {
  return (
    <div className="my-info">
      <img src={LogoWhite} alt="logo" />
      <h4>
        Entra en el mundo del desarrollo web, disfruta creando proyectos de todo
        tipo, deja que tu imaginación fluya y crea maravillas.
      </h4>
      <SocialLinks />
    </div>
  );
}
