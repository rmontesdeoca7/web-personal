import React from "react";
import "./NavigationFooter.scss";
import { Row, Col, Icon } from "antd";
import { Link } from "react-router-dom";

export default function NavigationFooter() {
  return (
    <Row className="navigation-footer">
      <Col>
        <h3>Navegación</h3>
      </Col>
      <Col md={12}>
        <RenderListLeft />
      </Col>
      <Col md={12}>
        <RenderListRight />
      </Col>
    </Row>
  );
}

function RenderListLeft() {
  return (
    <ul>
      <li>
        <a href="#">
          <Icon type="book" /> Cursos Online
        </a>
      </li>
      <li>
        <a href="#">
          <Icon type="database" /> Base de Datos
        </a>
      </li>
      <li>
        <Link to="/contact">
          <Icon type="code" /> Desarrollo web
        </Link>
      </li>
      <li>
        <a href="#">
          <Icon type="right" /> Política de Privacidad
        </a>
      </li>
    </ul>
  );
}

function RenderListRight() {
  return (
    <ul>
      <li>
        <a href="#">
          <Icon type="hdd" /> Sistemas
        </a>
      </li>
      <li>
        <a href="#">
          <Icon type="appstore" /> CMS
        </a>
      </li>
      <li>
        <a href="#">
          <Icon type="user" /> Portafólio
        </a>
      </li>
      <li>
        <a href="#">
          <Icon type="right" /> Política de Cookies
        </a>
      </li>
    </ul>
  );
}
