const Course = require("../models/course");

function addCourse(req, res) {
  const body = req.body;
  const course = new Course(body);

  course.order = 1000;
  course.save((err, courseSaved) => {
    if (err)
      res.status(400).send({ code: 400, message: "Error al crear el curso" });
    else {
      if (!courseSaved)
        res
          .status(404)
          .send({ code: 404, message: "No se ha podido crear el curso" });
      else
        res
          .status(200)
          .send({ code: 200, message: "Curso creado correctamente" });
    }
  });
}

function getCourses(req, res) {
  Course.find()
    .sort({ order: "asc" })
    .exec((err, coursesStored) => {
      if (err)
        res.status(400).send({ code: 400, message: "Error del servidor" });
      else {
        if (!coursesStored)
          res.status(404).send({
            code: 404,
            message: "No se ha podido consultar los cursos"
          });
        else res.status(200).send({ code: 200, courses: coursesStored });
      }
    });
}

function deleteCourse(req, res) {
  const { id } = req.params;
  Course.findByIdAndRemove(id, (err, courseDelete) => {
    if (err)
      res.status(500).send({ code: 5500, message: "Error del servidor" });
    else {
      if (!courseDelete)
        res.status(404).send({
          code: 404,
          message: "No se ha podido borrar el curso."
        });
      else
        res
          .status(200)
          .send({ code: 200, message: "Curso borrado exitosamente." });
    }
  });
}

function updateCourse(req, res) {
  const { id } = req.params;
  const courseData = req.body;

  Course.findByIdAndUpdate(id, courseData, (err, courseUpdated) => {
    if (err)
      res.status(500).send({ code: 5500, message: "Error del servidor" });
    else {
      if (!courseUpdated)
        res.status(404).send({
          code: 404,
          message: "No se ha podido borrar el curso."
        });
      else
        res.status(200).send({
          code: 200,
          message: "Curso actualizado exitosamente.",
          courseUpdated
        });
    }
  });
}

module.exports = {
  addCourse,
  getCourses,
  deleteCourse,
  updateCourse
};
