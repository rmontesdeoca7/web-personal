const Newsletter = require("../models/newsletter");

function suscribeEmail(req, res) {
  const { email } = req.params;
  const newsletter = new Newsletter();

  if (!email)
    res.status(404).send({ code: 404, message: "El correo es obligatorio." });
  else {
    newsletter.email = email.toLowerCase();
    newsletter.save((err, newsletterSaved) => {
      if (err)
        res.status(500).send({ code: 500, message: "Error en el server" });
      else {
        if (!newsletterSaved)
          res
            .status(404)
            .send({ code: 404, message: "Error al crear el newsletter" });
        else
          res
            .status(200)
            .send({ code: 200, message: "Newsletter creado correctamente" });
      }
    });
  }
}

module.exports = {
  suscribeEmail
};
