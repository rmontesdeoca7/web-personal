const Menu = require("../models/menu");

function addMenu(req, res) {
  const { title, url, order, active } = req.body;
  const menu = new Menu();
  menu.title = title;
  menu.url = url;
  menu.order = order;
  menu.active = active;

  menu.save((err, createMenu) => {
    if (err) res.status(500).send({ message: "Error en el server" });
    else {
      if (!createMenu)
        res.status(404).send({ message: "Error al crear el menu" });
      else res.status(200).send({ message: "Menu creado correctamente" });
    }
  });
}

function getMenus(req, res) {
  Menu.find()
    .sort({ order: "asc" })
    .exec((err, menus) => {
      if (err) res.status(500).send({ message: "Error en el server" });
      else {
        if (!menus)
          res.status(404).send({ message: "Error no se encuentra el menu" });
        else res.status(200).send({ menu: menus });
      }
    });
}

function updateMenu(req, res) {
  let menuData = req.body;
  const params = req.params;

  Menu.findByIdAndUpdate({ _id: params.id }, menuData, (err, menuUpdate) => {
    if (err) res.status(500).send({ message: "Error del servidor" });
    else {
      if (!menuUpdate)
        res.status(404).send({ message: "No se ha encontrado el menu" });
      else res.status(200).send({ message: "Menu actualizado correctamente" });
    }
  });
}

function activateMenu(req, res) {
  const { id } = req.params;
  let { active } = req.body;

  Menu.findByIdAndUpdate(id, { active }, (err, menuUpdate) => {
    if (err) res.status(500).send({ message: "Error del servidor" });
    else {
      if (!menuUpdate)
        res.status(404).send({ message: "No se ha encontrado el menu" });
      else {
        if (active)
          res.status(200).send({ message: "Menu activado correctamente" });
        else
          res.status(200).send({ message: "Menu desactivado correctamente" });
      }
    }
  });
}

function deleteMenu(req, res) {
  const { id } = req.params;

  Menu.findByIdAndRemove(id, (err, menuDeleted) => {
    if (err) res.status(500).send({ message: "Error del servidor" });
    else {
      if (!menuDeleted)
        res.status(404).send({ message: "No se ha encontrado el menu" });
      else res.status(200).send({ message: "Menu borrado correctamente." });
    }
  });
}

module.exports = {
  addMenu,
  getMenus,
  updateMenu,
  activateMenu,
  deleteMenu
};
