const mongoose = require("mongoose");
const app = require("./app");
const { API_VERSION, IP_SERVER, PORT_DB, PORT_SERVER } = require("./config");

// conection db
mongoose.set("useFindAndModify", false);

mongoose.connect(
  // `mongodb://${IP_SERVER}:${PORT_DB}/web_personal`,
  `mongodb+srv://mongo_user_db:ProyectoMERN-WEB-Personal-2021@webpersonal.v3hal.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err, res) => {
    if (err) throw err;
    else console.log("La conexión a la db es correcta");

    app.listen(PORT_SERVER, () => {
      console.log("###### API REST ########");
      console.log(`http://${IP_SERVER}:${PORT_SERVER}/api/${API_VERSION}`);
    });
  }
);
